'use strict';

/**
 * Theme. ScrollToID.
 * ---------------------------------------------------------------------------------------------------------------------
 */

function extJS_scrollToID() {
	const button = $('.ext-scroll');
	const container = $('html,body');

	button.on('click', (function (e) {
		e.preventDefault();
		container.animate({
			scrollTop: $(this.hash).offset().top
		}, 500);
	}));
}

/**
 * Theme. ScrollToTop.
 * ---------------------------------------------------------------------------------------------------------------------
 */

function extJS_scrollToTop() {
	$(window).on('scroll', function () {
		const button = '#scrollToTop';

		if ($(this).scrollTop() > 400) {
			$(button).fadeIn();
		} else {
			$(button).fadeOut();
		}
	});
}

/**
 * Theme. Clipboard.
 * ---------------------------------------------------------------------------------------------------------------------
 */

function extJS_clipBoard() {
	const elClipboard = '.ext-clipboard';
	const clipboard = new ClipboardJS(elClipboard);

	clipboard.on('success', function (e) {
		e.clearSelection();
	});
}

/**
 * Theme. Payment Target.
 * ---------------------------------------------------------------------------------------------------------------------
 */

function extJS_paymentTarget(file) {
	const elCommonList = $('.paymentTargetCommonList');
	const elProjectList = $('.paymentTargetProjectList');

	$.getJSON(file, function (data) {
		const get = data.target;
		const common = get.common;
		const project = get.project;

		let i = 0, j = 0;
		const len_i = common.length, len_j = project.length;

		for (; i < len_i; i++) {
			elCommonList.append($('<option />').attr('value', common[i].value).text(common[i].title));
		}

		for (; j < len_j; j++) {
			elProjectList.append($('<option />').attr('value', project[j].value).text(project[j].title));
		}
	});
}

/**
 * Theme. Field Validation.
 * ---------------------------------------------------------------------------------------------------------------------
 */

function extJS_fieldValidation() {
	const elForm = $('form');
	const elField = 'input[required]';

	elForm.find(elField).on('keydown keyup change', function () {
		if ($(this).val().length > 0) {
			$(this).removeClass('is-danger').addClass('is-success');
		} else {
			$(this).removeClass('is-success').addClass('is-danger');
		}
	});
}

/**
 * Theme. Particles JS.
 * ---------------------------------------------------------------------------------------------------------------------
 */

function extJS_particlesJS(id, file) {
	particlesJS.load(id, file);
}

/**
 * Theme. Page Indicator.
 * ---------------------------------------------------------------------------------------------------------------------
 */

function extJS_pageIndicator(status = 'loading') {
	let icon, out;

	switch (status) {
		case 'loading':
			icon = 'fas fa-cog fa-spin';
			break;
		case 'success':
			icon = 'fas fa-check-circle';
			break;
		case 'error':
			icon = 'fas fa-exclamation-triangle';
			break;
		case 'complete':
			icon = 'fas fa-check-circle';
			break;
		default:
			icon = '';
	}

	out = '<div id="page-indicator-' + status + '" class="page-indicator"><span class="icon"><i class="' + icon + ' fa-lg"></i></span></div>';

	$('.page-indicator').remove();
	$('body').append(out);
}